import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../models/Post';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  // http://localhost:8080/api/v1/posts is our link

  private getUrl: string = "http://localhost:8080/api/v1/posts";

  // using dependency injection, bring in the HttpClient
  constructor(private httpClient: HttpClient) { }

  // method that will make a request to the GetMapping method in our
  // backend
  getPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(this.getUrl).pipe(
      map(result => result)
    )
  }

  // method that will make a request to the post mapping () in our backend
  savePost(newPost: Post): Observable<Post> {
    return this.httpClient.post<Post>(this.getUrl, newPost);
  }

  // method that will make a request to the GetMapping method
  // to view an individual Post from our backend

  viewPost(id: number): Observable<Post> {
    return this.httpClient.get<Post>(`${this.getUrl}/${id}`).pipe(
      map(result => result)
    )
  }

  // method that will make a request to the DeleteMapping method to
  // delete an individual post from our backend
  deletePost(id: number): Observable<any> {
    return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: 'text'})
  }



}
